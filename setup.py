import warnings

from distutils.core import setup

package = __import__('theme')

try:
    README = open('README.md').read()
except:
    warnings.warn('Could not read README.md')
    README = None

try:
    REQUIREMENTS = open('requirements.txt').read()
except:
    warnings.warn('Could not read requirements.txt')
    REQUIREMENTS = None


setup(
    name='webmvc-pro-base-theme',
    version="1.1",
    description=(
        'Django-SHOP base theme for Web.mvc Pro.'
    ),
    long_description=README,
    install_requires=REQUIREMENTS,
    author='Danny Waser',
    author_email='danny@waser.tech',
    url='https://gitlab.waser.tech/web.mvc-themes/web.mvc-pro-base-theme',
    packages=[
        'theme',
        'theme.migrations',
    ],
    include_package_data=True,
    classifiers=[
        'Development Status :: 6 - Mature',
        'Environment :: Web Environment',
        'Framework :: Django, Django-SHOP',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Utilities'
    ],
)
