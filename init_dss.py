import os
import pickle

def module_path(module_name):
    module = __import__(module_name.split(".")[0])
    path = os.path.dirname(module.__file__)
    return path

apps = ["commodity", "theme"]
dss = [app + "/settings.dss" for app in apps]
apps += ["newsletter", "faq"]

DS_SETTINGS = {'settings':{'INSTALLED_APPS':apps}, 'load_dss':dss, 'urlpatterns':[]}
main_settings_path = "settings.dss"
main_file = open(main_settings_path, 'wb')
pickle.dump(DS_SETTINGS, main_file)
main_file.close()

for app in dss:
    dss_src_absolute = os.path.join(module_path(app.split("/")[0]), app.split("/")[1])
    if os.path.isfile(dss_src_absolute.replace(".dss", ".py")):
        app_settings = __import__(app.split("/")[0] + "." + app.split("/")[1].split(".")[0])
        app_dss_file = open(dss_src_absolute, 'wb')
        pickle.dump(app_settings.settings.DSS, app_dss_file)
        app_dss_file.close()