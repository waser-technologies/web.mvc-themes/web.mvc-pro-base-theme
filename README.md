# Web.mvc Pro Base Theme

Base theme for [Web.mvc Pro](https://gitlab.waser.tech/technologies/web.mvc-pro). You can use it to make your own themes with Bootstrap 4.

## Installation
### Clone this repository
`git clone git@gitlab.com:waser-technologies/web.mvc-themes/web.mvc-pro-base-theme.git`
### Create environment variables
`cp .env.exemple .env`

Remember to edit the variables inside `.env` file before continuing.
#### Allow direnv to export variables
Install [direnv](https://direnv.net) and allow the root folder of the project.

`direnv allow .`
### Initiate Web.mvc
```
make env
make init
```
## Usage
### Developpement server
You can start a local developpment server to test your theme. Try to add pages, content and articles to see the restults of you work.

`make server`
## Theme
### File structure

```
Web.mvc Base Theme
├── Web.mvc/ #Core
├── faq/ #FAQ app
├── newsletter/ #Newsletter app
├── theme/ # Theme app
├── ├── locale/
├── ├── migrations/
├── ├── static/ # Static sorage
├── ├── ├── bootstrap/
├── ├── ├── css/
├── ├── ├── ├── **/*.css
├── ├── ├── img/
├── ├── ├── ├── favicon.png
├── ├── ├── js/
├── ├── ├── ├── **/*.js
├── ├── templates/
├── ├── apps.py
├── ├── cms_plugins.py # CMS components
├── ├── context_processors.py
├── ├── models.py
├── ├── settings.py # Theme settings
├── .dockerignore
├── .env.exemple
├── .envrc
├── .gitignore
├── Dockerfile
├── docker-compose.yml
├── LICENSE
├── Makefile
├── README.md
└── requirements.txt
```

#### Settings
+ HAS_PURCHASED_THEME_LICENCE

    Default :
    `True`

    You can tell Web.mvc if this theme has a purchasable licence to remove footer backlink.

    `True if os.environ.get('HAS_PURCHASED_THEME_LICENCE', False) == "true" else False`

+ THEME_EDITOR_SETTINGS

    Default :
    ```
    {
        'stylesSet': [
            {'name': "Feature title", 'element': "h1", 'attributes': {'class': "intro-title"}},
            {'name': "Feature subtitle", 'element': "p", 'attributes': {'class': "intro-subtitle"}},        
            {'name': "Section title", 'element': "h3", 'attributes': {'class': "title-a text-center"}},
            {'name': "Section title 2", 'element': "h5", 'attributes': {'class': "title-left title-box-2"}},
            {'name': "Section subtitle", 'element': "p", 'attributes': {'class': "subtitle-a text-center"}},
            {'name': "Footer title", 'element': "h5", 'attributes': {'class': "text-white"}},
            {'name': "Small", 'element': "small"},
            {'name': "Lead", 'element': "P", 'attributes': {'class': "lead"}},
            {'name': "Badge Green", 'element': "span", 'attributes': {'class': "badge badge-success"}},
            {'name': "Text Slider items", 'element': "span", 'attributes': {'class': "text-slider-items"}},
            {'name': "Text Slider", 'element': "strong", 'attributes': {'class': "text-slider"}},
            {'name': "Section line", 'element': "div", 'attributes': {'class': "line-mf"}},
            {'name': "Code", 'element': "code"},
            {'name': "Code block", 'element': "pre"},
            {'name': "Scrollable code block", 'element': "pre"},
            {'name': "Variable", 'element': "var"},
            {'name': "User Input", 'element': "kbd"},
            {'name': "Sample Output", 'element': "samp"},
        ],
    }
    ```
    This is the settings for the text editor. You can set text styles to quickly access them from the CMS text editior.

    Get more informations about this settings [@djangocms-text-ckeditor](https://github.com/divio/djangocms-text-ckeditor).

+ THEME_FORMS_TEMPLATES

    default :
    ```
    (
        ('Forms/default.html', 'Default'),
    )
    ```

    This settings controls the templates for the Forms.

    Get more informations about this settings [@djangocms-forms](https://github.com/mishbahr/djangocms-forms#configuration).

+ THEME_TEMPLATES

    default :
    ```
    (
        ('page.html', _('Blank page')),
        ('page_footer.html', _('Blank page with Footer')),
        ('feature.html', _('Page with Feature')),
        ('feature_footer.html', 'Page with Feature et Footer'),
    )
    ```

    This settings allows you to set templates for the CMS.

    You can get more information about this settings [@django-cms](http://docs.django-cms.org/en/latest/how_to/templates.html).

+ THEME_IMAGE_FULL_SIZE

    default :
    ```
    {'size': '370x541', 'crop': True, 'upscale': False}
    ```

    Get more informations about blog images [@djangocms-blog](https://djangocms-blog.readthedocs.io/en/latest/settings.html).

+ THEME_IMAGE_THUMBNAIL_SIZE

    default :
    ```
    {'size': '340x200', 'crop': True, 'upscale': False}
    ```

    This sets the default image thumbnail options for blog articles minature.
    
    Get more informations about blog thumbnails [@djangocms-blog](https://djangocms-blog.readthedocs.io/en/latest/settings.html).

+ THEME_ICON_SETS

    default :
    ```
    [
        ('fontawesome4', 'fa', 'Font Awesome'),
    ]
    ```
    Allows you to add other icons for the CMS.

    Get more info about this setting [@djangocms-icons](https://djangocms-blog.readthedocs.io/en/latest/settings.html).
+ THEME_BLOG_LATEST_POSTS
    
    default :
    `3`
    
    Number of blog posts for latest blog component.

    Get more informations about blog components [@djangocms-blog](https://djangocms-blog.readthedocs.io/en/latest/settings.html).

+ THEME_BLOG_PLUGIN_TEMPLATE_FOLDERS

    default :

    ```
    (
        ("plugins", _("Default template")),
    )
    ```
    Templates for blog components.
    
    Get more informations about blog components [@djangocms-blog](https://djangocms-blog.readthedocs.io/en/latest/settings.html).

#### Models
##### Components
Sometimes you want to build some theme specific component for the CMS.

For this purpose you can create [Django-CMS plugins](http://docs.django-cms.org/en/latest/how_to/custom_plugins.html).
##### Apply modifications
Remember to make migrations and migrate them to the database, when you modify the models.
```
make migrations
make migrate
```
#### Template API
```
Web.mvc Base Theme
├── theme/
├── ├── templates/
├── ├── ├── admin/
├── ├── ├── djangocms_blog/
├── ├── ├── ├── includes/
├── ├── ├── ├── plugins/
├── ├── ├── ├── base.html
├── ├── ├── ├── post_detail.html
├── ├── ├── ├── post_list.html
├── ├── ├── faq/
├── ├── ├── Form/
├── ├── ├── newsletter/
├── ├── ├── base.html
├── ├── ├── page.html
├── ├── ├── footer.html
├── ├── ├── feature.html
└────────── feature_footer.html
```
The templates folder contains every html templates for the site.

Web.mvc uses Django templates to render dynamic CMS pages. Templates are rendered using Jinja2 to make use of logic and variables inside them.

More information about templates [@Django](https://docs.djangoproject.com/en/2.2/topics/templates/) and [@Django-CMS](http://docs.django-cms.org/en/release-3.5.x/how_to/templates.html).

+ `base.html`

    This is the base of every templates. It define the strucure of all other templates.

    Every templates contains `title`, `feature`, `content`, and `footer` blocks.

+ `page.html`

    This is the default blank page template for the CMS.

+ `footer.html`

    This is a blank page with footer.

+ `feature.html`

    This is a page with feature for content.

+ `feature_footer.html`

    This is page with feature and footer.

+ `djangocms_blog/*.html`

    Those templates are used for the blog app.

    Get more information about blog templates [@djangocms-blog](https://djangocms-blog.readthedocs.io/en/latest/settings.html).

+ `faq/*.html`

    Those are the templates used for FAQ app.

+ `Forms/*.html`

    Those are the forms app templates.

    Get more informations about those templates [@djangocms-forms](https://github.com/mishbahr/djangocms-forms#configuration).

+ `newsletter/*.html`

    Those are the newsletter app templates.

##### Bootstrap 4
To streamline the process of adding new components to the CMS, we choosed [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/). 

It also allows you to easily create your theme from already made templates or from scratch.
#### Deploy Web.mvc
Web.mvc is powered by Docker. You can easily create your server within minutes.
##### docker-compose
Install [docker-compose](https://docs.docker.com/compose/install/) and compose your server. 

Remeber to edit `config/web_mvc`, `config/nginx.conf` and `config/postgres` files.

`docker-compose up --build -d`

This will spawn up Web.mvc along with a Postgres database and a NGINX server.

You also need to migrate the database and collect static files.

```
docker-compose run --rm web_mvc /web_mvc/manage.py migrate
docker-compose run --rm web_mvc /web_mvc/manage.py collectstatic --no-input
```

Or simply use `make compose` to compose the server, migrate the database and collect static files.

If it's the first time you run the server you may want to create an administrator account.

```
docker-compose run --rm web_mvc /web_mvc/manage.py createsuperuser"
```
##### Docker image
Addtionaly, you may want to only host an image of Web.mvc and manage the database and nginx nodes yourself.

You would need an [Docker Hub](https://hub.docker.com) free account and to change thoses variables accordingly inside the .env file.

```
DOCKER_USER
DOCKER_PASSWORD
DOCKER_REPO
```
Now you can push your image to your docker repository.

`make image`
#### Web.mvc Cloud
Waser Technologies host Web.mvc on [Jelastic Cloud](https://jelastic.com) with [Infomaniak](https://www.infomaniak.com/en/hosting/dedicated-and-cloud-servers/jelastic-cloud). But there is a large source of Jelastic providers worldwide.

Using [Cloud.API](https://gitlab.com/waser-technologies/technologies/cloud.api) it's easy to start a new Web.mvc server on your own Jelastic account.

Start by installing Cloud.API and [python-nginx](https://github.com/peakwinter/python-nginx) inside your environment.

```
source env/bin/activate
pip install git+https://gitlab.com/waser-technologies/technologies/cloud.api.git
pip install python-nginx
```

You need to publish a docker image of your theme first.

Just remeber to edit the .env file to change the value of `API_PLATEFORM` and `API_TOKEN`.

`make jelastic`

>>>
### You really need a website now ?
Use [Web.mvc Monthly](https://waser.tech/en-en/technologies/license/monthly/) to get a running website in minutes without any code, using our [themes collection](https://waser.tech/themes/).

You also get free support and the ability to request new features.
>>>
