from django.conf import settings # import the settings file

def theme_licence(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {'IS_LICENCED': settings.HAS_PURCHASED_THEME_LICENCE}