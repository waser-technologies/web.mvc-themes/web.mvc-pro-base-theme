from django.urls import reverse_lazy
from django.utils.text import format_lazy
from django.utils.translation import ugettext_lazy as _
import os.path

DSS = {
    'settings': {
        'HAS_PURCHASED_THEME_LICENCE': True, #True if os.environ.get('HAS_PURCHASED_THEME_LICENCE', False) == "true" else False,
        'LOCALE_PATHS': ["theme/locale"],
        'CKEDITOR_SETTINGS': {    
            'stylesSet': format_lazy('default:{}', reverse_lazy('admin:cascade_texteditor_config')),
        },
        'TEMPLATES': {
            'DIRS': ["theme/templates"],
            'OPTIONS': {
                'context_processors': ['theme.context_processors.theme_licence'],
            },
        },
        'DJANGOCMS_FORMS_TEMPLATES': [
            ('Forms/default.html', 'Default'),
            ('Forms/default-invert.html', 'Clean'),
        ],
        'CMS_TEMPLATES': [
            ## Customize this
            ('page.html', _('Page blanche')),
            ('page_footer.html', _('Page blanche avec Pied de page')),
            ('sidebar_right.html', 'Barre latérale droite'),
            ('sidebar_right_footer.html', 'Barre latérale droite avec Pied de page'),
            ('sidebar_left.html', _('Barre latérale gauche')),
            ('sidebar_left_footer.html', 'Barre latérale gauche avec Pied de page'),
            ('feature.html', _('Feature')),
            ('feature_footer.html', 'Feature and Footer'),
        ],
        'BLOG_IMAGE_FULL_SIZE': {'size': '370x541', 'crop': True, 'upscale': False},
        'BLOG_IMAGE_THUMBNAIL_SIZE': {'size': '340x200', 'crop': True, 'upscale': False},
        'DJANGOCMS_ICON_SETS': [
            ('fontawesome5regular', 'far', 'Font Awesome Regular'),
            ('fontawesome5solid', 'fas', 'Font Awesome 5 Solid'),
            ('fontawesome5brands', 'fab', 'Font Awesome 5 Brands'),
        ],
        'BLOG_LATEST_POSTS': 3,
        'BLOG_PLUGIN_TEMPLATE_FOLDERS': [
            ("plugins", _("Default template")),
        ],
        'NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS': [
            ("plugins", _("Default template")),
        ],
    }
}