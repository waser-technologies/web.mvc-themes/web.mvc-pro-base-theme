from django.db import models
from django.utils.translation import ugettext_lazy as _

from cms.models import CMSPlugin
from filer.fields.image import FilerImageField, FilerFileField

# Create your models here.
class BackgroundImage(CMSPlugin):
    name = models.CharField(_('target_class'), max_length=50, help_text=_("Assign a CSS class, e.g. '.footer-img'."))
    image = FilerImageField(verbose_name=_('image'), blank=False, null=False, related_name='image')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Background image')
        verbose_name_plural = _('Background image')