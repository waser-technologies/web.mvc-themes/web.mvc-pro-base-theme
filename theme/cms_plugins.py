from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _

from .models import BackgroundImage

@plugin_pool.register_plugin
class BackgroundImagePlugin(CMSPluginBase):
    name = _('Background Image')
    module = _('Feature')
    model = BackgroundImage
    render_template = 'bg_img.html'
    cache = True
    allow_children = False

@plugin_pool.register_plugin
class SectionPlugin(CMSPluginBase):
    model = CMSPlugin
    name = _("Section")
    module = _("Bootstrap 4")
    render_template = "section_plugin.html"
    cache = False
    allow_children = True