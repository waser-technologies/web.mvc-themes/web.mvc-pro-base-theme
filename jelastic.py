from cloud_api import CloudAPI
import nginx
import hashlib
import random
import time
import os


sitename = os.env.get("SITE_NAME")

username = os.env.get("USER_NAME")
userpassword = os.env.get("USER_PASSWORD")
usermail = os.env.get("USER_EMAIL")
userfirstname = os.env.get("USER_FIRSTNAME")
userlastname = os.env.get("USER_LASTNAME")

langs = os.env.get("LANGUAGES")
tz = os.env.get("SITE_TIMEZONE")
isDebug = os.env.get("DEBUG")

emailHost = os.env.get("EMAIL_HOST")
emailUser = os.env.get("EMAIL_USER")
emailPassword = os.env.get("EMAIL_PASSWORD")
emailDefault = os.env.get("EMAIL_DEFAULT")
emailVerification = os.env.get("EMAIL_VERIFICATION")

docker_user = os.env.get("DOCKER_USER")
docker_password = os.env.get("DOCKER_PASSWORD")
docker_image = os.env.get("DOCKER_IMAGE")
docker_repo = os.env.get("DOCKER_REPO")

ng = "bl"
dom = os.env.get("DOMAIN_NAME")
domains = [dom, ", www." + dom]
ssl_settings = {
    'nodeGroup': ng,
    'customDomains': ".".join([dom]),
}

cloud = CloudAPI(platform=API_PLATEFORM, token=API_TOKEN)
s = "web-mvc".join(["waser.tech", "waser"])
env = {
        'displayName': "Web.mvc",
        'shortdomain': "env-" + str(int(hashlib.sha256(s.encode('utf-8')).hexdigest(), 16) % 10**7),
        'sslstate':True,
    }

nodes = [
            {
                "count": 1,
                "displayName": "Equilibrage",
                "docker": {
                    "nodeGroup": "bl",
                },
                "extip": True,
                "fixedCloudlets": 1,
                "flexibleCloudlets": 2,
                "nodeType": "nginx",

            },
            {
                "count": 1,
                "displayName": "Web.mvc",
                "docker": {
                    "nodeGroup": "cp",
                    "image": docker_image,
                    "registry": {
                        "password": docker_password,
                        "url": docker_repo,
                        "user": docker_user,
                    },
                },
                "extip": False,
                "fixedCloudlets": 1,
                "flexibleCloudlets": 6,
                "nodeType" : "docker",
            },
            {
                "count": 1,
                "displayName": "Base de données PostgresQL",
                "docker": {
                    "nodeGroup": "sqldb",
                },
                "extip": False,
                "fixedCloudlets": 1,
                "flexibleCloudlets": 2,
                "nodeType": "postgresql",
            },
            {
                "count": 1,
                "displayName": "RabbitMQ",
                "docker": {
                    "nodeGroup": "nosqldb",
                    "image": "rabbitmq:latest"
                },
                "extip": False,
                "fixedCloudlets": 1,
                "flexibleCloudlets": 4,
                "nodeType" : "docker",
            },
    ]

Env = cloud.createEnvironment(env=env, nodes=nodes, skipNodeEmails=True)
cloud.setEnvGroup(env=Env, envGroup="Waser Technologies")

cloud.jpsInstall(jps="https://raw.githubusercontent.com/jelastic-jps/lets-encrypt/master/manifest.jps", env=Env, settings=ssl_settings, nodeGroup=ssl_settings.get('nodeGroup'), skipNodeEmails=True)
time.sleep(5)
cloud.setEnvDomain(env=Env, domain=dom)

#delay to let the server finish processing
time.sleep(30)

ngNodeId = Env.getNodesByGroup(nodeGroup=ng)[0].get('id')
cloud.addVolume(env=Env, source_path="/mnt/www/data/static", source_node=ngNodeId, destination_path="/static", nodeGroup="cp")
cloud.addVolume(env=Env, source_path="/mnt/www/data/media", source_node=ngNodeId, destination_path="/media", nodeGroup="cp")

appNodeIp = Env.getNodesByGroup(nodeGroup="cp")[0].get('intIP')

nginxConf = nginx.Conf()

httpServ = nginx.Server()
httpServ.add(
    nginx.Key('listen', '80'),
    nginx.Key('listen', '[::]:80'),
    nginx.Key('server_name', dom + " www." + dom),
    nginx.Key('return', 'https://{0}$request_uri'.format(dom)),
)
nginxConf.add(httpServ)

ssl = nginx.Server()
ssl.add(
        nginx.Key('listen', '443 http2'),
        nginx.Key('listen', '[::]:443 http2'),
        nginx.Key('server_name', dom + " www." + dom),
        nginx.Key('error_log', "/var/log/nginx/error_log info"),
        nginx.Key('access_log', "/var/log/nginx/access_log main"),

        nginx.Key('ssl', 'on'),
        nginx.Key('ssl_certificate', '/var/lib/jelastic/SSL/jelastic.chain'),
        nginx.Key('ssl_certificate_key', '/var/lib/jelastic/SSL/jelastic.key'),
        nginx.Key('ssl_session_timeout', '5m'),
        nginx.Key('ssl_protocols', 'TLSv1.2 TLSv1.3'),
        nginx.Key('ssl_ciphers', 'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384'),
        nginx.Key('ssl_prefer_server_ciphers', 'off'),
        nginx.Key('ssl_session_cache', 'shared:SSL:10m'),
        nginx.Key('proxy_temp_path', '/var/nginx/tmp/'),

        nginx.Location('/favicon.ico',
            nginx.Key('alias', '/mnt/www/data/static/img/favicon.ico'),
        ),

        nginx.Location('/static/',
            nginx.Key('autoindex', 'on'),
            nginx.Key('alias', '/mnt/www/data/static/'),
            nginx.Key('add_header', 'Cache-Control "public, max-age=31536000, immutable"'),
        ),
        nginx.Location('/media/',
            nginx.Key('alias', '/mnt/www/data/media/'),
            nginx.Key('autoindex', 'on'),
            nginx.Key('add_header', 'Cache-Control "public, max-age=31536000, immutable"'),
        ),
        nginx.Location('/',
            nginx.Key('proxy_pass', 'http://{0}:8000'.format(appNodeIp)),
            nginx.Key('proxy_http_version', '1.1'),
            nginx.Key('proxy_set_header', 'Upgrade $http_upgrade'),
            nginx.Key('proxy_set_header', 'Connection "upgrade"'),
            nginx.Key('proxy_set_header', 'Host $host'),
            nginx.Key('proxy_set_header', 'X-Real-IP $remote_addr'),
            nginx.Key('proxy_set_header', 'X-Forwarded-Host $host'),
            nginx.Key('proxy_set_header', 'X-Forwarded-For $http_x_forwarded_for'),
            nginx.Key('proxy_set_header', 'X-Host $http_host'),
            nginx.Key('proxy_set_header', 'X-URI $uri'),
            nginx.Key('proxy_set_header', 'X-ARGS $args'),
            nginx.Key('proxy_set_header', 'Refer $http_refer'),
            nginx.Key('proxy_set_header', 'X-Forwarded-Proto $scheme'),
            nginx.Key('proxy_set_header', 'Ssl-Offloaded "1"'),
        ),
    )
nginxConf.add(ssl)

cloud.createFile(env=Env, path="/etc/nginx/conf.d/web_mvc.conf", body=nginxConf.as_strings, nodeGroup="bl")
cloud.execCmdByGroup(env=Env, nodeGroup="bl", command="rm /etc/nginx/conf.d/ssl.conf")
time.sleep(10)
cloud.restartNodes(env=Env, nodeGroup="bl")
time.sleep(10)
#configure databases
# PostgreSQL
sqlNodeGroup = "sqldb"
sqlNodeIP = Env.getNodesByGroup(nodeGroup=sqlNodeGroup)[0].get('intIP')


# create db password
dbp = "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789") for i in range(32)])

# change connection permisions to allow connection
cloud.execCmdByGroup(env=Env, nodeGroup=sqlNodeGroup, command="rm /var/lib/pgsql/data/pg_hba.conf")

pg_hba_conf = [
    "local   all         postgres                          peer",
    "local	all			all								  md5",
    "host    all         all         127.0.0.1/32          ident",
    "host    all         all         ::1/128               ident",
    "host    all         all         0.0.0.0/0             md5",
    "host	all			all			{0}/32		  trust".format(str(appNodeIp)),
]

cloud.createFile(env=Env, path="/var/lib/pgsql/data/pg_hba.conf", body=pg_hba_conf, nodeGroup=sqlNodeGroup)

cloud.restartNodes(env=Env, nodeGroup=sqlNodeGroup)

#delay to let node restart
time.sleep(60)

# psql create user
cloud.execCmdByGroup(env=Env, nodeGroup=sqlNodeGroup, command="""psql -c "CREATE USER web_mvc WITH PASSWORD '%s'" """ % dbp)
time.sleep(10)
cloud.execCmdByGroup(env=Env, nodeGroup=sqlNodeGroup, command="""psql -c "ALTER ROLE web_mvc SET client_encoding TO 'utf8'" """)
time.sleep(10)
cloud.execCmdByGroup(env=Env, nodeGroup=sqlNodeGroup, command="""psql -c "ALTER ROLE web_mvc SET default_transaction_isolation TO 'read committed'" """)
time.sleep(10)
cloud.execCmdByGroup(env=Env, nodeGroup=sqlNodeGroup, command="""psql -c "ALTER ROLE web_mvc SET timezone TO '%s'" """ % tz)
time.sleep(10)
# psql create database
cloud.execCmdByGroup(env=Env, nodeGroup=sqlNodeGroup, command="""psql -c "CREATE DATABASE web_mvc" """)
time.sleep(10)
# psql grant database privileges to user
cloud.execCmdByGroup(env=Env, nodeGroup=sqlNodeGroup, command="""psql -c "GRANT ALL PRIVILEGES ON DATABASE web_mvc TO web_mvc" """)
time.sleep(10)
cloud.createFile(env=Env, path="/var/spool/cron/postgres", body=["0 0 * * * /var/lib/jelastic/bin/backup_script.sh -m dump -c 10 -u {0} -p {1} -d web_mvc".format("web_mvc" , dbp)], nodeGroup=sqlNodeGroup)

# RabbitMQTT
rmqtth = Env.getNodesByGroup(nodeGroup="nosqldb")[0].get('intIP')

rmqttp = "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789") for i in range(32)])

cloud.execCmdByGroup(env=Env, nodeGroup="nosqldb", command="""rabbitmqctl add_user webmvc '%s'""" % rmqttp)
time.sleep(10)
cloud.execCmdByGroup(env=Env, nodeGroup="nosqldb", command="""rabbitmqctl set_permissions webmvc \".*\" \".*\" \".*\"""")

rmqtt = "amqp://webmvc:" + rmqttp + "@" + str(rmqtth) + ":5672/"

# Deploy docker
sk = "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789") for i in range(48)])
extIP = cloud.getExtIP(env=Env)[0]
time.sleep(5)
#set ENV variables
environment_vars = {
    'SECRET_KEY': sk,
    'ALLOWED_HOSTS': "".join([extIP, ", .", dom, ", 127.0.0.1"]),
    'DEBUG': isDebug,
    'POSTGRES_HOST': sqlNodeIP,
    'POSTGRES_NAME': "web_mvc",
    'POSTGRES_USER': "web_mvc",
    'POSTGRES_PASSWORD': dbp,
    'BROKER_URL': rmqtt,
    'LANGUAGES': langs,
#    'GOOGLE_ANALYTICS_PROPERTY_ID': analyticsID,
    'EMAIL_HOST': emailHost,
    'EMAIL_HOST_USER': emailUser,
    'EMAIL_HOST_PASSWORD': emailPassword,
    'DEFAULT_FROM_EMAIL': emailDefault,
    'ACCOUNT_EMAIL_VERIFICATION': emailVerification,
    'SITE_NAME': sitename,
}

cloud.addContainerEnvVars(env=Env, envVars=environment_vars)
time.sleep(10)
cloud.execCmdByGroup(env=Env, nodeGroup="cp", command="python /web_mvc/manage.py migrate")
time.sleep(10)
newSiteCmd = """echo "from django.contrib.sites.models import Site; a = Site.objects.get(pk=1); a.domain = '{0}'; a.name = '{1}'; a.save()" | python /web_mvc/manage.py shell""".format(dom, sitename)
cloud.execCmdByGroup(env=Env, nodeGroup="cp", command=newSiteCmd)
time.sleep(10)
newSuperUserCmd = """echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('{0}', '{1}', '{2}', first_name='{3}', last_name='{4}')" | python /web_mvc/manage.py shell""".format(username, usermail, userpassword, userfirstname, userlastname)
cloud.execCmdByGroup(env=Env, nodeGroup="cp", command=newSuperUserCmd)
time.sleep(10)
cloud.execCmdByGroup(env=Env, nodeGroup="cp", command="python /web_mvc/manage.py collectstatic --noinput")
time.sleep(10)
cloud.restartNodes(env=Env, nodeGroup="cp")
print("\n"*5)
print("Set a type A relation to " + str(extIP))