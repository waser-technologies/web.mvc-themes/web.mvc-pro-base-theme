from decimal import Decimal
import os  # isort:skip
import six
import pickle
from django.urls import reverse_lazy
from django.utils.text import format_lazy
from django.utils.translation import ugettext_lazy as _
from cmsplugin_cascade.bootstrap4.mixins import BootstrapUtilities
from cmsplugin_cascade.extra_fields.config import PluginExtraFieldsConfig

from .utils import load_dss

gettext = lambda s: s
DATA_DIR = os.path.dirname(os.path.dirname(__file__))

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DS_SETTINGS_FILENAME = "settings.dss"

# Shop

SHOP_APP_LABEL = 'commodity'
SHOP_MONEY_FORMAT = "{code} {amount}"

SHOP_VALUE_ADDED_TAX = Decimal(7.7)
SHOP_DEFAULT_CURRENCY = 'CHF'
SHOP_EDITCART_NG_MODEL_OPTIONS = "{updateOn: 'default blur', debounce: {'default': 2500, 'blur': 0}}"

if os.environ.get('SAFERPAY_USERNAME', None):
    SHOP_SAFERPAY_URL = 'https://test.saferpay.com/api' if os.environ.get('SAFERPAY_TEST') == True else 'https://test.saferpay.com/api'
    SHOP_CUSTOMER_ID = os.environ.get('SAFERPAY_USERNAME')
    SHOP_SAFERPAY_PASSWORD = os.environ.get('SAFERPAY_PASSWORD')
    SHOP_CUSTOMER_ID = os.environ.get('SAFERPAY_CUSTOMERID') # a number
    SAFERPAY_TERMINALID = os.environ.get('SAFERPAY_TERMINALID')  # a number
    SAFERPAY_SUCCESS_CAPTURE_URL = reverse_lazy('payments:capture')  # your callback after getting the money
    SAFERPAY_SUCCESS_URL = reverse_lazy('payments:done')  # your callback after a successfull order
    SAFERPAY_FAIL_URL = reverse_lazy('payments:canceled')  # your callback after a failed payment
    SAFERPAY_ORDER_TEXT_NR = _('Payment for ' + os.environ.get('SITE_NAME', SAFERPAY_USERNAME) + ', Order nr. %s.')
    SAFERPAY_FORCE_LIABILITYSHIFT_ACTIVE = False  # default: False
    SAFERPAY_DO_NOTIFY = True if os.environ.get('SAFERPAY_TEST') == True else False # default: False
    SHOP_REFUND_URL = SHOP_SAFERPAY_URL + "/Payment/v1/Transaction/Refund"

SHOP_CART_MODIFIERS = [
    'commodity.modifiers.PrimaryCartModifier',
    'shop.modifiers.taxes.CartExcludedTaxModifier',
    'commodity.modifiers.PostalShippingModifier',
#    'commodity.modifiers.SaferPayModifier',
    'shop.payment.modifiers.PayInAdvanceModifier',
    'shop.shipping.modifiers.SelfCollectionModifier',
]

SHOP_ORDER_WORKFLOWS = [
    'shop.payment.workflows.ManualPaymentWorkflowMixin',
    'shop.payment.workflows.CancelOrderWorkflowMixin',
    'shop.shipping.workflows.PartialDeliveryWorkflowMixin',
    'commodity.providers.OrderWorkflowMixin',
]

SHOP_CASCADE_FORMS = {
    'CustomerForm': 'commodity.forms.CustomerForm',
}

THUMBNAIL_HIGH_RESOLUTION = True
FILE_UPLOAD_PERMISSIONS = 0o644
FILE_UPLOAD_MAX_MEMORY_SIZE = 10000000 #10Mo
FILER_ADMIN_ICON_SIZES = ('16', '32', '48', '80', '128')
FILER_ALLOW_REGULAR_USERS_TO_ADD_ROOT_FOLDERS = True
FILER_DUMP_PAYLOAD = False
THUMBNAIL_OPTIMIZE_COMMAND = {
    'gif': '/usr/bin/optipng {filename}',
    'jpeg': '/usr/bin/jpegoptim {filename}',
    'png': '/usr/bin/optipng {filename}'
}

THUMBNAIL_PRESERVE_EXTENSIONS = True

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False if str(os.environ.get('DEBUG', "false")) == "false" else True

ALLOWED_HOSTS = [host for host in os.environ.get('ALLOWED_HOSTS', "" if DEBUG == False else "*").replace(", ", ",").split(",")] #[]

# Application definition
ROOT_URLCONF = 'Web_mvc.urls'

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

TIME_ZONE = os.environ.get('TIME_ZONE', "Europe/Zurich")
USE_THOUSAND_SEPARATOR = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(DATA_DIR, 'static')
MEDIA_ROOT = os.path.join(DATA_DIR, 'media')

STATICFILES_FINDERS = [
    # or 'django.contrib.staticfiles.finders.FileSystemFinder',
    'commodity.finders.FileSystemFinder',
    # or 'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'commodity.finders.AppDirectoriesFinder',
    'sass_processor.finders.CssFinder',
]

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'Web_mvc', 'static'),
    ('node_modules', os.path.join(BASE_DIR, 'node_modules')),
)

SITE_ID = 1


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'Web_mvc', 'templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.media',
                'django.template.context_processors.csrf',
                'django.template.context_processors.tz',
                'sekizai.context_processors.sekizai',
                'django.template.context_processors.static',
                'cms.context_processors.cms_settings',
            ],
            'libraries': {
                'sorl_thumbnail':'sorl.thumbnail.templatetags.thumbnail',
                'easy_thumbnails': 'easy_thumbnails.templatetags.thumbnail',
            },
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                'django.template.loaders.eggs.Loader'
            ],
        },
    },
    {
    'BACKEND': 'post_office.template.backends.post_office.PostOfficeTemplates',
    'APP_DIRS': True,
    'DIRS': [],
    'OPTIONS': {
        'context_processors': [
            'django.contrib.auth.context_processors.auth',
            'django.template.context_processors.debug',
            'django.template.context_processors.i18n',
            'django.template.context_processors.media',
            'django.template.context_processors.static',
            'django.template.context_processors.tz',
            'django.template.context_processors.request',
        ]
    }
}
]

POST_OFFICE = {
    'TEMPLATE_ENGINE': 'post_office',
}

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'select2': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

REDIS_HOST = os.getenv('REDIS_HOST')
if REDIS_HOST:
    SESSION_ENGINE = 'redis_sessions.session'

    SESSION_REDIS = {
        'host': REDIS_HOST,
        'port': 6379,
        'db': 0,
        'prefix': 'session-',
        'socket_timeout': 1
    }

    CACHES['default'] = {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': 'redis://{}:6379/1'.format(REDIS_HOST),
        'OPTIONS': {
            'PICKLE_VERSION': 2 if six.PY2 else -1,
        }
    }

    CACHE_MIDDLEWARE_ALIAS = 'default'
    CACHE_MIDDLEWARE_SECONDS = 3600
else:
    SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'

SESSION_SAVE_EVERY_REQUEST = True

MIDDLEWARE_CLASSES = (
    'cms.middleware.utils.ApphookReloadMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware'
)

INSTALLED_APPS = [
    'djangocms_admin_style',
    'django.contrib.auth',
    'allauth',
    'polymorphic',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'analytical',
    'crispy_forms',
    'cmsplugin_cascade',
    'cmsplugin_cascade.clipboard',
    'cmsplugin_cascade.sharable',
    'cmsplugin_cascade.extra_fields',
    'cmsplugin_cascade.icon',
    'cmsplugin_cascade.segmentation',
    'cms_bootstrap',
    'adminsortable2',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',
    'django_fsm',
    'fsm_admin',
    'djng',
    'cms',
    'menus',
    'sekizai',
    'treebeard',
    'sass_processor',
    'django_filters',
    'djangocms_text_ckeditor',
    'filer',
    'easy_thumbnails',
    'easy_thumbnails.optimize',
    'sorl.thumbnail',
    'djangocms_column',
    'djangocms_file',
    'djangocms_link',
    'djangocms_picture',
    'djangocms_style',
    'djangocms_snippet',
    'djangocms_googlemap',
    'djangocms_video',
    'djangocms_icon',
    'djangocms_bootstrap4',
    'djangocms_bootstrap4.contrib.bootstrap4_alerts',
    'djangocms_bootstrap4.contrib.bootstrap4_badge',
    'djangocms_bootstrap4.contrib.bootstrap4_card',
    'djangocms_bootstrap4.contrib.bootstrap4_carousel',
    'djangocms_bootstrap4.contrib.bootstrap4_collapse',
    'djangocms_bootstrap4.contrib.bootstrap4_content',
    'djangocms_bootstrap4.contrib.bootstrap4_grid',
    'djangocms_bootstrap4.contrib.bootstrap4_jumbotron',
    'djangocms_bootstrap4.contrib.bootstrap4_link',
    'djangocms_bootstrap4.contrib.bootstrap4_listgroup',
    'djangocms_bootstrap4.contrib.bootstrap4_media',
    'djangocms_bootstrap4.contrib.bootstrap4_picture',
    'djangocms_bootstrap4.contrib.bootstrap4_tabs',
    'djangocms_bootstrap4.contrib.bootstrap4_utilities',
    'aldryn_apphooks_config',
#    'cmsplugin_filer_image',
    'parler',
    'taggit',
    'taggit_autosuggest',
    'meta',
    'sortedm2m',
    'djangocms_page_meta',
    'djangocms_blog',
    'djangocms_forms',
    'post_office',
    'haystack',
    'shop',
    'Web_mvc',
]

LANGS = os.environ.get("LANGUAGES", "fr-fr").replace(", ", ",").split(",")

USE_I18N = True #True if len(LANGS) > 1 else False

USE_L10N = USE_I18N

LANGUAGES = []
for lang in LANGS:
    LANGUAGES.append((lang, gettext(lang)))

LANGUAGE_CODE = LANGS[0]

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

META_SITE_PROTOCOL = 'https'
META_USE_SITES = True

DATABASES =  {
    'default': {
        'ENGINE': "django.db.backends.sqlite3",
        'NAME': os.path.join(DATA_DIR, 'db.sqlite3'),
    }
}

LOCALE_PATHS = []

CMS_LANGUAGES = {
    'default': {
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': False,
    },
}

CMS_LANGUAGES[1] = []

for lg in LANGS:
    CMS_LANGUAGES[1].append({
            'code': lg,
            'hide_untranslated': False,
            'name': lg,
            'public': True,
        })

if len(LANGS) > 1:
    CMS_LANGUAGES[1][1]['redirect_on_fallback'] = True

DJANGOCMS_FORMS_PLUGIN_NAME = _('Form')

CRISPY_TEMPLATE_PACK = 'bootstrap4'

DJANGOCMS_FORMS_WIDGET_CSS_CLASSES = {'__all__': ('form-control', ) }

CKEDITOR_SETTINGS = {
    'stylesSet': format_lazy('default:{}', reverse_lazy('admin:cascade_texteditor_config')),
}

CMS_PERMISSION = bool(os.environ.get('CMS_PERMISSION', False))

CMS_PLACEHOLDER_CONF = {}

FILER_FILE_MODELS = (
      'filer.Image',
      'filer.File',
)


CMS_CACHE_DURATIONS = {
    'content': 600,
    'menus': 3600,
    'permissions': 86400,
}


CMS_PLACEHOLDER_CONF = {
    'Breadcrumb': {
        'plugins': ['BreadcrumbPlugin'],
        'parent_classes': {'BreadcrumbPlugin': None},
    },
    'Commodity Details': {
        'plugins': ['BootstrapContainerPlugin', 'BootstrapJumbotronPlugin'],
        'parent_classes': {
            'BootstrapContainerPlugin': None,
            'BootstrapJumbotronPlugin': None,
        },
    },
    'Main Content': {
        'plugins': ['BootstrapContainerPlugin', 'BootstrapJumbotronPlugin'],
        'parent_classes': {
            'BootstrapContainerPlugin': None,
            'BootstrapJumbotronPlugin': None,
            'TextLinkPlugin': ['TextPlugin', 'AcceptConditionPlugin'],
        },
    },
    'Static Footer': {
        'plugins': ['BootstrapContainerPlugin', 'BootstrapJumbotronPlugin'],
        'parent_classes': {
            'BootstrapContainerPlugin': None,
            'BootstrapJumbotronPlugin': None,
        },
    },
}

CMSPLUGIN_CASCADE_PLUGINS = [
    'cmsplugin_cascade.bootstrap4',
    'cmsplugin_cascade.segmentation',
    'cmsplugin_cascade.generic',
    'cmsplugin_cascade.icon',
    'cmsplugin_cascade.leaflet',
    'cmsplugin_cascade.link',
    'shop.cascade',
]

CMSPLUGIN_CASCADE = {
    'link_plugin_classes': [
        'shop.cascade.plugin_base.CatalogLinkPluginBase',
        'cmsplugin_cascade.link.plugin_base.LinkElementMixin',
        'shop.cascade.plugin_base.CatalogLinkForm',
    ],
    'alien_plugins': ['TextPlugin', 'TextLinkPlugin', 'AcceptConditionPlugin'],
    'bootstrap4': {
        'template_basedir': 'angular-ui',
    },
    'plugins_with_extra_render_templates': {
        'CustomSnippetPlugin': [
            ('shop/catalog/product-heading.html', _("Product Heading")),
            ('commodity/catalog/Brand-filter.html', _("Brand Filter")),
        ],
        # required to purchase real estate
        'ShopAddToCartPlugin': [
            (None, _("Default")),
            ('commodity/catalog/commodity-add2cart.html', _("Add Commodity to Cart")),
        ],
    },
    'plugins_with_sharables': {
        'BootstrapImagePlugin': ['image_shapes', 'image_width_responsive', 'image_width_fixed',
                                 'image_height', 'resize_options'],
        'BootstrapPicturePlugin': ['image_shapes', 'responsive_heights', 'image_size', 'resize_options'],
    },
    'plugins_with_extra_fields': {
        'BootstrapCardPlugin': PluginExtraFieldsConfig(),
        'BootstrapCardHeaderPlugin': PluginExtraFieldsConfig(),
        'BootstrapCardBodyPlugin': PluginExtraFieldsConfig(),
        'BootstrapCardFooterPlugin': PluginExtraFieldsConfig(),
        'SimpleIconPlugin': PluginExtraFieldsConfig(),
    },
    'plugins_with_extra_mixins': {
        'BootstrapContainerPlugin': BootstrapUtilities(BootstrapUtilities.background_and_color),
        'BootstrapRowPlugin': BootstrapUtilities(BootstrapUtilities.paddings),
        'BootstrapYoutubePlugin': BootstrapUtilities(BootstrapUtilities.margins),
        'BootstrapButtonPlugin': BootstrapUtilities(BootstrapUtilities.floats),
    },
    'leaflet': {
        'tilesURL': 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
        'accessToken': 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
        'apiKey': 'AIzaSyD71sHrtkZMnLqTbgRmY_NsO0A9l9BQmv4',
    },
    'bookmark_prefix': '/',
    'segmentation_mixins': [
        ('shop.cascade.segmentation.EmulateCustomerModelMixin',
         'shop.cascade.segmentation.EmulateCustomerAdminMixin'),
    ],
    'allow_plugin_hiding': True,
}

ELASTICSEARCH_HOST = os.environ.get('ELASTICSEARCH_HOST', 'localhost')

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://{}:9200/'.format(ELASTICSEARCH_HOST),
        'INDEX_NAME': 'commerce-en',
    },
    'fr': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://{}:9200/'.format(ELASTICSEARCH_HOST),
        'INDEX_NAME': 'commerce-fr',
    }
}

HAYSTACK_ROUTERS = [
    'shop.search.routers.LanguageRouter',
]

SELECT2_CSS = 'node_modules/select2/dist/css/select2.min.css'
SELECT2_JS = 'node_modules/select2/dist/js/select2.min.js'

if not os.environ.get('DEV', False):
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {'require_debug_false': {'()': 'django.utils.log.RequireDebugFalse'}},
        'formatters': {
            'simple': {
                'format': '[%(asctime)s %(module)s] %(levelname)s: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'simple',
            },
            'file': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'formatter': 'simple',
                'filename': '/var/log/Web_mvc.log',
            },
        },
        'loggers': {
            'django': {
                'handlers': ['file', 'console'],
                'level': 'INFO',
                'propagate': True,
            },
            'post_office': {
                'handlers': ['file', 'console'],
                'level': 'WARNING',
                'propagate': True,
            },
        },
    }

SILENCED_SYSTEM_CHECKS = ['auth.W004']

FIXTURE_DIRS = [
    os.path.join(DATA_DIR, 'fixtures'),
]

SEND_GRID_API_KEY = ''
EMAIL_HOST = os.environ.get('EMAIL_HOST')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = os.environ.get('DEFAULT_FROM_EMAIL')
ACCOUNT_EMAIL_SUBJECT_PREFIX = ''
EMAIL_BACKEND = 'post_office.EmailBackend'

NODE_MODULES_URL = STATIC_URL + 'node_modules/'

SASS_PROCESSOR_INCLUDE_DIRS = [
    os.path.join(BASE_DIR, 'node_modules'),
]

COERCE_DECIMAL_TO_STRING = True

FSM_ADMIN_FORCE_PERMIT = True

ROBOTS_META_TAGS = ('noindex', 'nofollow')

SERIALIZATION_MODULES = {'json': str('shop.money.serializers')}


REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': [
        'shop.rest.money.JSONRenderer',
        # can be disabled for production environments
        'rest_framework.renderers.BrowsableAPIRenderer',
    ],
    'DEFAULT_FILTER_BACKENDS': [
        'django_filters.rest_framework.DjangoFilterBackend',
    ],
    # 'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    # 'PAGE_SIZE': 16,
}

REST_AUTH_SERIALIZERS = {
    'LOGIN_SERIALIZER': 'shop.serializers.auth.LoginSerializer',
}


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

ACCOUNT_AUTHENTICATION_METHOD = "email"
ACCOUNT_EMAIL_REQUIRED=True
ACCOUNT_EMAIL_VERIFICATION= os.environ.get('ACCOUNT_EMAIL_VERIFICATION', "mandatory")
ACCOUNT_DEFAULT_HTTP_PROTOCOL="https"
ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE=True
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = "account/login"

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'Web_mvc/locale'),
]
# os.path.join(BASE_DIR, 'theme/locale')

BLOG_PERMALINK_URLS = {
    'slug': r'^(?P<slug>\w[-\w]*)/$',
    'full_date': r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/(?P<slug>\w[-\w]*)/$',
    'short_date': r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<slug>\w[-\w]*)/$',
    'category': r'^(?P<category>\w[-\w]*)/(?P<slug>\w[-\w]*)/$',
}

FILER_FILE_MODELS = (
    'filer.Image',
    'filer.File',
)


# Dynamicly stored settings
DS_SETTINGS = load_dss(BASE_DIR, DS_SETTINGS_FILENAME)

if DS_SETTINGS != None:
    INSTALLED_APPS += DS_SETTINGS['settings'].get('INSTALLED_APPS')
    LOCALE_PATHS += [os.path.join(BASE_DIR, local_path) for local_path in DS_SETTINGS['settings'].get('LOCALE_PATHS')]
    DJANGOCMS_FORMS_TEMPLATES = DS_SETTINGS['settings'].get('DJANGOCMS_FORMS_TEMPLATES')
    NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS = DS_SETTINGS['settings'].get('NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS')
    TEMPLATES[0]['DIRS'] += [os.path.join(BASE_DIR, local_path) for local_path in DS_SETTINGS['settings']['TEMPLATES'].get('DIRS')]
    TEMPLATES[0]['OPTIONS']['context_processors'] += DS_SETTINGS['settings']['TEMPLATES']['OPTIONS'].get('context_processors', [])
    BLOG_IMAGE_FULL_SIZE = DS_SETTINGS['settings'].get('BLOG_IMAGE_FULL_SIZE')
    BLOG_IMAGE_THUMBNAIL_SIZE = DS_SETTINGS['settings'].get('BLOG_IMAGE_THUMBNAIL_SIZE')
    BLOG_LATEST_POSTS = DS_SETTINGS['settings'].get('BLOG_LATEST_POSTS')
    BLOG_PLUGIN_TEMPLATE_FOLDERS = DS_SETTINGS['settings'].get('BLOG_PLUGIN_TEMPLATE_FOLDERS')
    CMS_TEMPLATES = DS_SETTINGS['settings'].get('CMS_TEMPLATES')
    CKEDITOR_SETTINGS = DS_SETTINGS['settings'].get('CKEDITOR_SETTINGS', None) if DS_SETTINGS['settings'].get('CKEDITOR_SETTINGS', None) != None else CKEDITOR_SETTINGS
    DJANGOCMS_ICON_SETS = DS_SETTINGS['settings'].get('DJANGOCMS_ICON_SETS')
    HAS_PURCHASED_THEME_LICENCE = DS_SETTINGS['settings'].get('HAS_PURCHASED_THEME_LICENCE')
